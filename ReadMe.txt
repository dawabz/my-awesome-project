Hi and thanks for reading this. 
I'm Pat (theCodeBunny) and I'm from Switzerland.
I'm currently working in IT doing troubleshooting and fixing systems
but I've always been more interested in the software part and specially
development. As my life is already quite busy  I'm still learning at my own
pace and this can feel very slow and frustrating at times. But I haven't give
up and I'm definitly not going to either. I'm more than motivated to push some
code into the octocat and contribute at my humble level to the most projects
out there.
Thanks for reading and happy coding !  :)